<?php

/**
 * @file
 * External help for the External help module.
 */

/**
 * Implements hook_externalhelp().
 */
function externalhelp_externalhelp() {
  return array(
    'help_import' => array(
      'label' => t('About having external help as inline help'),
      'url' => 'http://drupal.org/node/1031972',
    ),
    'online_docs' => array(
      'label' => t('Shape of future Drupal online documentation'),
      'url' => 'http://drupal.org/node/1291058',
    ),
  );
}
